package main

import (
	"fmt"
	"log"
	"os"
	"reflect"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
	gripui "gitlab.com/deal.justin/grip-ui/internal"
	"gitlab.com/deal.justin/grip/pkg/grib"
)

func main() {
	app := tview.NewApplication()

	// load the input grib file using GRIP
	var filename = os.Args[1]
	messages, err := grib.Scan(filename)
	if err != nil {
		log.Fatalf("Failed to scan GRIB2 file: %s", err)
	}

	// GRIB view shows us the available messages and is the entry point
	gribView := gripui.NewGribView(messages)
	gribView.SetDoneFunc(func() {
		app.Stop()
	})

	activeSection := 0
	activeField := 0

	// on select, we'll switch to a message view specific to the selected message
	gribView.SetSelectedFunc(func(i int, _ string, _ string, _ rune) {

		// grab the GRIB message we'll be inspecting
		msg := messages[i]

		hexPane := gripui.NewHexPane(filename)
		logPane := gripui.NewLogPane()
		octetsPane := gripui.NewOctetsPane(msg)
		sectionsPane := gripui.NewSectionsPane()
		overviewPane := gripui.NewOverviewPane(msg)

		// on left/right we'll change the active section
		octetsPane.SetSection(activeSection)

		// do it once to initialize the view
		hexPane.Render(activeSection, activeField, msg, logPane, filename, app, gribView, hexPane)

		// this is the layout for the message view
		flex := tview.NewFlex().
			AddItem(hexPane, 82, 1, false).
			AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
				AddItem(tview.NewFlex().SetDirection(tview.FlexColumn).
					AddItem(sectionsPane, 0, 1, false).
					AddItem(overviewPane, 0, 1, false),
					11, 1, false).
				AddItem(octetsPane, 0, 2, false).
				AddItem(logPane, 0, 1, false),
				0, 1, false)

		flex.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {

			fmt.Fprintf(logPane, "test")

			key := event.Key()
			if key == tcell.KeyRight {
				// go forward one section
				if activeSection < 8 {
					activeSection++
					activeField = 0
				}
			} else if key == tcell.KeyLeft {
				// go back one section
				if activeSection > 0 {
					activeSection--
					activeField = 0
				}
			} else if key == tcell.KeyESC {
				app.SetRoot(gribView, true)
				app.SetFocus(gribView)
			}

			sectionsPane.ActiveSection = activeSection
			sectionsPane.Render()

			sectionName := fmt.Sprintf("S%d", activeSection)
			sectionElem := reflect.ValueOf(&msg).Elem().FieldByName(sectionName)
			numFields := sectionElem.Type().NumField()

			if key == tcell.KeyUp {
				if activeField > 0 {
					activeField--
				}
			} else if key == tcell.KeyDown {
				if activeField < numFields-1 {
					activeField++
				}
			}

			// update octets view and set active sections color
			octetsPane.SetOctet(activeField)
			octetsPane.SetSection(activeSection)

			hexPane.Render(activeSection, activeField, msg, logPane, filename, app, gribView, hexPane)

			return event
		})

		app.SetRoot(flex, true)
		app.SetFocus(flex)
	})

	// fire it on up
	app.SetRoot(gribView, true)
	app.SetFocus(gribView)
	if err := app.Run(); err != nil {
		panic(err)
	}
}
