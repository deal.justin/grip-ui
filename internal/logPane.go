package gripui

import (
	"github.com/rivo/tview"
)

// LogPane exists for debugging mostly, but provides a place on screen to drop messages
type LogPane struct {
	*tview.TextView
}

// NewLogPane returns a new LogPane
func NewLogPane() *LogPane {

	pane := LogPane{tview.NewTextView()}

	pane.SetBorder(true)
	pane.SetTitle(" log ")

	return &pane
}
