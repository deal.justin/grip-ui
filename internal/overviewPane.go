package gripui

import (
	"fmt"

	"github.com/rivo/tview"
	"gitlab.com/deal.justin/grip/pkg/grib"
)

// OverviewPane shows some of the high level message details
type OverviewPane struct {
	*tview.TextView
	message grib.Message
}

// NewOverviewPane returns a new OverviewPane
func NewOverviewPane(message grib.Message) *OverviewPane {

	pane := OverviewPane{tview.NewTextView(), message}
	pane.SetBorder(true)
	pane.SetTitle(" overview ")
	pane.SetDynamicColors(true)

	pane.Render()

	return &pane
}

// Render the list of sections
func (pane *OverviewPane) Render() {
	pane.Clear()

	fmt.Fprintf(pane, " Length:     [#%06x]%d bytes[white]\n", sectionColors[0], pane.message.S0.Length)
	fmt.Fprintf(pane, " Ref. Time:  [#%06x]%v[white]\n", sectionColors[1], pane.message.S1.GetDate())

	param := pane.message.S4.GetParameter(pane.message.S0.Discipline)
	fmt.Fprintf(pane, " Discipline: [#%06x]%s[white]\n", sectionColors[0], pane.message.S0.GetDisipline())
	fmt.Fprintf(pane, " Parameter:  [#%06x]%s (%s)[white]\n", sectionColors[4], param.Description, param.Abbreviation)
	fmt.Fprintf(pane, " Level:      [#%06x]%s[white]\n", sectionColors[4], pane.message.S4.GetLevel())

	hasLocalUse := false
	if pane.message.S2.Length > 0 {
		hasLocalUse = true
	}
	fmt.Fprintf(pane, " Local Use:  [#%06x]%v[white]\n", sectionColors[2], hasLocalUse)
}
