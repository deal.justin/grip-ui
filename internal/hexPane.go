package gripui

import (
	"fmt"
	"os"
	"reflect"
	"strconv"
	"strings"

	"github.com/rivo/tview"
	"gitlab.com/deal.justin/grip/pkg/grib"
)

// HexPane provides a hexdump style, highlighted view of the GRIB file.
type HexPane struct {
	*tview.TextView
	file *os.File
}

// NewHexPane prepares a new HexPane from the provided filename
func NewHexPane(filename string) *HexPane {

	var file, err = os.Open(filename)
	if err != nil {
		panic("Failed to open the GRIB file!")
	}

	view := HexPane{tview.NewTextView(), file}
	view.SetBorder(true)
	view.SetTitle(" hexdump ")
	view.SetDynamicColors(true)

	return &view
}

// Render the hex pane
func (pane *HexPane) Render(
	activeSection int,
	activeField int,
	msg grib.Message,
	logPane *LogPane,
	filename string,
	app *tview.Application,
	list *GribView,
	hexPane *HexPane) {

	hexPane.Clear()
	logPane.Clear()

	// figure out the current tag. tags should look like this:
	//		`octets: "1-4"`
	//		`octets: "7"`
	fromInt := 0
	thruInt := 0
	sectionName := fmt.Sprintf("S%d", activeSection)
	sectionElem := reflect.ValueOf(&msg).Elem().FieldByName(sectionName)
	tag, ok := sectionElem.Type().Field(activeField).Tag.Lookup("octet")
	if ok {
		pieces := strings.Split(tag, "-")
		fromInt, _ = strconv.Atoi(pieces[0])
		if len(pieces) > 1 {
			thruInt, _ = strconv.Atoi(pieces[1])
		} else {
			thruInt = fromInt
		}
	}

	// these tell us the bytes to highlight relative to the section start (in bytes) which is
	// kept track of in each section
	offsetName := fmt.Sprintf("S%dOffset", activeSection)
	offsetElem := reflect.ValueOf(&msg).Elem().FieldByName(offsetName)
	offset := offsetElem.Int()
	length := int64(sectionElem.FieldByName("Length").Uint())
	if sectionName == "S0" {
		length = 16
	}
	fromByte := offset + int64(fromInt) - 1 // docs start counting at 1 instead of 0
	thruByte := offset + int64(thruInt) - 1 // docs start counting at 1 instead of 0

	fmt.Fprintf(logPane, "%s is from %d to %d. active octets are from %d to %d\n", sectionName, offset, offset+length, fromByte, thruByte)

	// seek to this message in the input file
	var file, err = os.Open(filename)
	if err != nil {
		app.Stop()
	}

	file.Seek(msg.ByteOffsetInFile, 0)

	// now we update the hexdump view. replicating `hexdump -C` like the encoding/hex package does with its Dump()
	// output, but we want to fit in highlighting, so we'll need to do this ourselves. Not hard at least.
	//
	// encoding/hex package has a nice view of the hexdump output:
	// 		00000010  2e 2f 30 31 32 33 34 35  36 37 38 39 3a 3b 3c 3d  |./0123456789:;<=|
	// 		^ offset                          ^ extra space              ^ ASCII of line.
	_, _, height, _ := list.GetRect()
	rows := height - 4
	totalBytes := 16 * rows

	data := make([]byte, totalBytes)
	file.Read(data)

	gray := "#606060"
	color := fmt.Sprintf("#%06x", sectionColors[activeSection])

	for row := 0; row < rows; row++ {
		rowOffset := int64(row * 16)
		rowData := data[rowOffset : rowOffset+16]

		// bytes and chars portions of the line
		bytes := ""
		chars := ""
		startHighlighting := false
		stillHighlighting := false
		stopHighlighting := false
		didHighlight := false
		for i, byteData := range rowData {

			// check if we should start highlighting
			thisByte := rowOffset + int64(i)
			if !stillHighlighting && thisByte >= fromByte && thisByte <= thruByte {
				startHighlighting = true
				stillHighlighting = true
				didHighlight = true
			}

			bytesFormat := ""
			charsFormat := ""

			// if we're starting highlighting, add the color code in
			if startHighlighting {
				bytesFormat += fmt.Sprintf("[%s::u]", color)
				charsFormat += fmt.Sprintf("[%s]", color)
				startHighlighting = false
			}

			// add the formatting rule
			bytesFormat += "%02x"
			charsFormat += "%c"

			// check if we should stop highlighting (we do this here even if we just started b/c we have 1 char wide octets)
			if stillHighlighting && (thisByte >= thruByte || fromByte == thruByte) {
				stillHighlighting = false
				stopHighlighting = true
			}

			// if we're stopping, turn off the highlighting
			if stopHighlighting {
				bytesFormat += fmt.Sprintf("[%s::-]", gray)
				charsFormat += fmt.Sprintf("[%s::-]", gray)
				stopHighlighting = false
			}

			bytesFormat += " "

			if i == 7 {
				bytesFormat += " "
			} else if i == 15 {
				bytesFormat += fmt.Sprintf("[%s]", gray)
				charsFormat += fmt.Sprintf("[%s]", gray)
			}

			bytes += fmt.Sprintf(bytesFormat, byteData)

			if byteData < 32 || byteData > 126 {
				byteData = '.'
			}

			chars += fmt.Sprintf(charsFormat, byteData)
		}

		// byte offset in file
		offsetColor := gray
		if didHighlight {
			offsetColor = color
		}
		fileOffset := fmt.Sprintf("[%s::-]%08x[%s::-] ", offsetColor, msg.ByteOffsetInFile+rowOffset, gray)

		// send to the view
		fmt.Fprintf(hexPane, " %s %s |%s|\n", fileOffset, bytes, chars)
	}

	file.Close()
}
