package gripui

import (
	"github.com/rivo/tview"
	"gitlab.com/deal.justin/grip/pkg/grib"
)

// GribView shows the available GRIB messages in a file
type GribView struct {
	*tview.List
}

// NewGribView returns a view that lists available GRIB messages
func NewGribView(messages []grib.Message) *GribView {

	view := GribView{tview.NewList()}
	view.ShowSecondaryText(false)

	letter := 'a'
	for i, msg := range messages {
		view.AddItem(msg.String(), "", letter+int32(i), nil)
	}

	return &view
}
