package gripui

import (
	"fmt"

	"github.com/rivo/tview"
)

var sectionNames = map[int]string{
	0: "Indicator",
	1: "Identification",
	2: "Local Use",
	3: "Grid (GDS)",
	4: "Product (PDS)",
	5: "Representation",
	6: "Bit Map",
	7: "Data",
	8: "End",
}
var sectionColors = map[int]int32{
	0: 0x21fff0,
	1: 0xffda49,
	2: 0xC42DA9,
	3: 0x257af9,
	4: 0x00ff00,
	5: 0xff8300,
	6: 0xbcbcbc,
	7: 0xea1551,
	8: 0x7547d8,
	9: 0x000000,
}

// SectionsPane exists for debugging mostly, but provides a place on screen to drop messages
type SectionsPane struct {
	*tview.TextView
	ActiveSection int
}

// NewSectionsPane returns a new SectionsPane
func NewSectionsPane() *SectionsPane {

	pane := SectionsPane{tview.NewTextView(), 0}
	pane.SetBorder(true)
	pane.SetTitle(" sections ")
	pane.SetDynamicColors(true)

	pane.Render()

	return &pane
}

// Render the list of sections
func (pane *SectionsPane) Render() {
	pane.Clear()

	for i := 0; i < 9; i++ {
		line := fmt.Sprintf("%d | [#%06x]%s[white]\n", i, sectionColors[i], sectionNames[i])
		if i == pane.ActiveSection {
			line = "* " + line
		} else {
			line = "  " + line
		}
		fmt.Fprintf(pane, line)
	}
}
