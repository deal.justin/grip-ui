package gripui

import (
	"fmt"
	"reflect"

	"github.com/rivo/tview"
	"gitlab.com/deal.justin/grip/pkg/grib"
)

// OctetsPane displays details from a grib.Message's section
type OctetsPane struct {
	*tview.TextView
	message grib.Message
	section int
	octet   int
}

// NewOctetsPane from a provided grib.Message
func NewOctetsPane(message grib.Message) OctetsPane {
	view := OctetsPane{tview.NewTextView(), message, 0, 0}

	view.SetBorder(true)
	view.SetTitle(" octets ")
	view.SetDynamicColors(true)

	return view
}

// SetSection to change the section being displayed
func (view *OctetsPane) SetSection(section int) {
	view.section = section

	view.Clear()

	sectionName := fmt.Sprintf("S%d", view.section)
	sectionElem := reflect.ValueOf(&view.message).Elem().FieldByName(sectionName)
	if !sectionElem.IsValid() {
		fmt.Fprintln(view, "no section data defined")
		return
	}

	sectionType := sectionElem.Type()

	// to aide in formatting, we'll loop through everything once and figure out our column widths
	maxTag, maxType, maxName, maxValue := 0, 0, 0, 0
	for i := 0; i < sectionElem.NumField(); i++ {
		field := sectionElem.Field(i)

		// skip unexported fields
		if !field.CanInterface() {
			continue
		}

		tag, ok := sectionElem.Type().Field(i).Tag.Lookup("octet")
		tagLength := len(tag)
		if ok && tagLength > maxTag {
			maxTag = tagLength
		}

		typeLength := len(field.Type().String())
		if typeLength > maxType {
			maxType = typeLength
		}

		nameLength := len(sectionType.Field(i).Name)
		if nameLength > maxName {
			maxName = nameLength
		}

		valueLength := len(fmt.Sprintf("%v", field.Interface()))
		if valueLength > maxValue {
			maxValue = valueLength
		}
	}

	fmt.Fprintf(view, "%*s %*s %*s %*s lookup\n", maxTag, "octet", maxName, "name", maxType, "type", maxValue, "value")
	fmt.Fprintln(view, "")
	for i := 0; i < sectionElem.NumField(); i++ {
		field := sectionElem.Field(i)
		fieldName := sectionType.Field(i).Name
		fieldType := field.Type()

		// skip unexported fields
		if !field.CanInterface() {
			continue
		}

		// grab the octet tag
		tag, ok := sectionElem.Type().Field(i).Tag.Lookup("octet")
		if !ok {
			tag = "unknown"
		}

		// array values we'll replace with a stand in
		value := field.Interface()
		if fieldType.Kind() == reflect.Slice || fieldType.Kind() == reflect.Array {
			value = "[ ... ]"
		}

		// big ugly formatting time
		color := "white"
		if i == view.octet {
			color = "red"
		}

		line := fmt.Sprintf("[%s]%10s[white] %*s %*s %*v", color, tag, maxName, fieldName, maxType, fieldType, maxValue, value)

		if view.section == 0 {
			if fieldName == "Discipline" {
				line += fmt.Sprintf(" %v", view.message.S0.GetDisipline())
			}
		}

		fmt.Fprintf(view, "%s\n", line)
	}
}

// SetOctet that is currently selected/highlighted
func (view *OctetsPane) SetOctet(octet int) {
	view.octet = octet
}
