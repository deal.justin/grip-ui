# GRIP UI

A terminal UI over [GRIP](https://gitlab.com/deal.justin/grip), the Golang GRIB Ripper.

# Build

This uses private repos, so it's a little bit awkward to get the dependency installed:

1. `git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"`
2. `GOPRIVATE=gitlab.com/deal.justin/grip go get gitlab.com/deal.justin/grip
3. `go build cmd/grip-ui/main.go`

# Run

Just pass in a GRIB file and you're good to go: `./main gfs.grib2`


# Theme

Overall
    * darker gray background
    * lighter gray borders
    * white text

Each section has its own color:
    * Section 0 - #21fff0 - Indicator       tealish
    * Section 1 - #ffda49 - Identification  yellowish
    * Section 2 - #C42DA9 - Local Use       fuschia
    * Section 3 - #257af9 - Grid (GDS)      blueish
    * Section 4 - #27ea58 - Product (PDS)   greenish
    * Section 5 - #ff8300 - Data Rep.       orangish
    * Section 6 - #bcbcbc - Bit Map         grayish
    * Section 7 - #ea1551 - Data            redish
    * Section 8 - #7547d8 - End             purpleish
