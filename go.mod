module gitlab.com/deal.justin/grip-ui

go 1.13

require github.com/rivo/tview v0.0.0-20200528200248-fe953220389f

require (
	github.com/gdamore/tcell v1.3.0
	gitlab.com/deal.justin/grip v0.0.0-20200706185158-5038832e6f9c
)
